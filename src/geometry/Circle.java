package geometry;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radius) {
		this.radius=radius;
	}
	
public double getRadius() {
	return this.radius;
}


public double getPerimeter(double a) {
	double perimeter = 2*(Math.PI)*(this.radius);
	return perimeter;
}

public double getArea(double p) {
	double area = (Math.PI)*(this.radius*this.radius); 
	return area;
}
public String toString() {
	return "The Area of the Circle is " + getArea(radius) 
	+ " and the Perimeter is " + getPerimeter(radius);
}
}
