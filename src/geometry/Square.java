package geometry;

public class Square extends Rectangle {
	public Square(double length) {
		super(length, length);
	}
	public String toString() {
		String square = super.toString();
		square= ("The Area of the Square is " + getArea(length) 
				+ " and the perimeter is " +getPerimeter(length));
		return square;
	}

}
