package geometry;

public interface Shape {
double getArea(double a);
double getPerimeter(double p);
}
