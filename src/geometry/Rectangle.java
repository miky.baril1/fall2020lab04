package geometry;

public class Rectangle implements Shape {
protected double length;
private double width;

public Rectangle(double length, double width) {
	this.length=length;
	this.width=width;
}
public double getLength() {
	return this.length;
}
public double getWidth() {
	return this.width;
}
public double getArea(double a) {
	double area = length*width;
	return area;
}
public double getPerimeter(double p) {
	double perimeter = (length*2)+(width*2);
	return perimeter;
}
public String toString() {
	return ("The Area of the Rectangle is " + getArea(length) 
			+ " and the perimeter is " +getPerimeter(length));
}
}
