package geometry;

public class LotsOfShapes {
public static void main (String[]args) {
	Shape[] shapes = new Shape [5];
	shapes[0]= new Rectangle(5,10);
	shapes[1]= new Rectangle(10,15);
	shapes[2]= new Circle(8);
	shapes[3]= new Circle(4);
	shapes[4]= new Square(10);
	for(int i=0;i<5;i++) {
		System.out.println(shapes[i]);
	}
}
}
