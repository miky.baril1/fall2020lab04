package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new Book("Sherlock","Book1");
		books[1]= new ElectronicBook("Einstein","Mathbook",105);
		books[2]= new Book("Miky","Basketballbook");
		books[3]= new ElectronicBook("Bro","Mathbook2",150.5);
		books[4]= new ElectronicBook("Brot","Mathbook3",136.9);
		for(int i=0;i<5;i++) {
			System.out.println(books[i]);
		}
	}

}
