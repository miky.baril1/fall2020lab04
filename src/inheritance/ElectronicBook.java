package inheritance;

public class ElectronicBook extends Book {
	protected double numberBytes;
	
	
	//Constructor
	public ElectronicBook(String author, String title, double numberBytes) {
		super(author, title);
		this.numberBytes=numberBytes;

	}
	public String toString() {
		String fromBase = super.toString();
		
		return (fromBase + " and there is : " + numberBytes + " bytes.");
		}
}

