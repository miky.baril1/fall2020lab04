package inheritance;

public class Book {
protected String title;
protected String author;

public Book(String author, String title) {
	this.author = author;
	this.title = title;
}

public String getTitle() {
	return this.title;
}
public String getAuthor() {
	return this.author;
}
//Constructor

public String toString() {
	return ("The title is : " + this.title + 
			" and the author is : " + this.author);
}
}
